<?php
use QI\Units\Unit\UnitInterface;

include  __DIR__ . "/vendor/autoload.php";
$elf=new \QI\Units\Unit\Elf(10,10);
$orc = new \QI\Units\Unit\Orc(15,15);
$elf->hit($orc);
echo "orc healthy:" .$orc->getHeals();
echo "\n";
$orc->hit($elf);
echo "\n";
echo "elf healthy:" .$elf->getHeals();
echo "\n";
echo "elf healthy after heals: " . $elf->acceptHeal(4);
echo "\n";

//echo $elf->hit(5);
//echo $elf->acceptHit(4);

//$ork=new \QI\Units\Unit\Orc();
//echo $ork->acceptHeal(12);
//echo "\n";

