<?php

namespace QI\Units\Unit;

class Orc extends Unit
{
    public function hit(UnitInterface $unit): int
    {
        $hit =8;
        $unit->acceptHit($hit);
        return $hit;
    }
}