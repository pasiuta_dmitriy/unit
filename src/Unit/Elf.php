<?php

namespace QI\Units\Unit;

class Elf extends Unit
{
    public function hit(UnitInterface $unit): int
    {
        $hit = 4;
        $unit->acceptHit($hit);
        return $hit;
    }
}