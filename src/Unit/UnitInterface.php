<?php
namespace QI\Units\Unit;
interface   UnitInterface{
public function hit(UnitInterface $unit);
public function acceptHit(int $hit);
public function acceptHeal(int $heals);
public function getHeals():int;

}