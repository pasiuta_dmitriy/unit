<?php

namespace QI\Units\Unit;
class Unit implements UnitInterface
{
    private $currentHealth;
    private $maxHealth;

    function __construct($currentHealth, $maxHealth)
    {
        $this->currentHealth = $currentHealth;
        $this->maxHealth = $maxHealth;
    }

    public function hit(UnitInterface $unit): int
    {
        $hit = 0;
        $unit->acceptHit($hit);
        return $hit;
    }

    public function acceptHit($hit)
    {
        if($this->currentHealth<=$hit)
        {
            $this->currentHealth=0;
        }


        else $this->currentHealth -= $hit;

    }

    public function acceptHeal(int $heals): int
    {
        if (($this->currentHealth + $heals) <= $this->maxHealth) {
            return $this->currentHealth += $heals;
        }

        return $this->maxHealth;
    }

    public function getHeals(): int
    {
        return $this->currentHealth;
    }

    public function getMaxHealth(){
        return $this->maxHealth;
    }
}
